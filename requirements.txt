Django==1.3.1
South==0.7.3
django-appconf==0.5
django-staticfiles==1.2.1
wsgiref==0.1.2
django-registration
