Click or copy the following URL into your browser to activate your new account:

http://{{ site.domain }}{% url registration.views.activate activation_key %}

(Note: you must activate your account within {{ expiration_days }} day(s) for it to become valid.)
