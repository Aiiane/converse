
Converse
========

Converse is an attempt to create a better forum system for ongoing discussions. It represents each topic (the equivalent of a typical forum's "thread") with an editable outline that supports commenting on each item in the outline, providing a more structured means of discussing a topic that inherently also provides an easily-accessible summary of past discussion.

Dependencies
------------
 * django
 * django-registration
 * south

An SMTP server must also be specified in settings.py for sending user activation emails.

Optional dependencies are:

 * django debug toolbar (if present, will be enabled for superusers)
 * django extensions (if present, will be loaded for use at the comment line)
