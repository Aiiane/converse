$(function() {
    Converse.topic = {};
    Converse.topic.focused_comment = null;
    Converse.topic.force_open_subtopic = false;

    Converse.topic.onclick_subtopic_header = function() {
        var that = $(this);
        var subtopic_id = /\d+/.exec(that.attr('id'))[0];
        if(Converse.topic.force_open_subtopic) {
            that.addClass('open');
            Converse.topic.force_open_subtopic = false;
        } else {
            that.toggleClass('open');
        }
        var convo = $('#subtopic-convo-' + subtopic_id);
        if(that.hasClass('open')) {
            convo.slideDown();
            $.proxy(Converse.topic.load_subtopic_convo_comments, convo)();
        } else {
            convo.slideUp();
        }
    };
    $('.subtopic').live('click', Converse.topic.onclick_subtopic_header);

    Converse.topic.onclick_subtopic_add_child = function() {
        var subtopic_id = /\d+/.exec($(this).closest('.subtopic').attr('id'))[0];
        $('#new-subtopic-' + subtopic_id).show();
        return false;
    };
    $('.add-child-link').live('click', Converse.topic.onclick_subtopic_add_child);

    Converse.topic.load_subtopic_convo_comments = function() {
        var that = $(this);
        var subtopic_id = /\d+/.exec(that.attr('id'))[0];
        var max_comment_id = that.find('.convo-items li').map(function() {
            return parseInt(/\d+/.exec($(this).attr('id'))[0]);
        }).get().reduce(function(x,y){return Math.max(x,y);}, 0);

        that.find('.bouncer').show();
        $.ajax({
            'url': Converse.base_url + 'subtopic/comments/' + subtopic_id + '/',
            'data': {'id_after': max_comment_id},
            'dataType': 'html',
            'success': function(html) {
                that.find('.bouncer').hide();
                if(html && html.trim()) {
                    $(html).appendTo(that.find('.convo-items')).slideDown(
                        'fast',
                        function() {
                            if(Converse.topic.focused_comment) {
                                $('.highlighted').removeClass('highlighted');
                                var el = that.find('#' + Converse.topic.focused_comment);
                                if(el.length) {
                                    el.addClass('highlighted');
                                    $(window).scrollTop(el.offset().top);
                                }
                                Converse.topic.focused_comment = null;
                            }
                        }
                    );
                }
                $('#subtopic-' + subtopic_id).removeClass('new');
            }
        });
    };

    Converse.topic.submit_comment_form = function() {
        var that = $(this);
        $.ajax({
            'url': that.attr('action'),
            'type': 'POST',
            'data': {
                'ajax': true,
                'content': that.find('textarea').val()
            },
            'dataType': 'json',
            'success': function(result) {
                if(result && result.success) {
                    $.proxy(
                        Converse.topic.load_subtopic_convo_comments,
                        that.closest('.subtopic-convo')
                    )();
                    that.find('textarea').val('');
                    that.find('.comment-errors').text('');
                } else {
                    that.find('.comment-errors').text(result.errors);
                }
            }
        });
        return false;
    };
    $('.comment-form').live('submit', Converse.topic.submit_comment_form);

    $('.comment-form textarea').autoResize({'extraSpace': 25});

    Converse.topic.show_topic_editor = function() {
        var summary = $('#topic-summary-editor');
        var title = $('#title-editor');
        $('.topic-summary').hide();
        $('#title-content').hide();
        $('.topic-title .edit-link').hide();
        title.attr('original', title.val());
        title.autoResize({'extraSpace': 50, 'maxWidth': 700}).data('AutoResizer').check();
        summary.attr('original', summary.val());
        summary.autoResize({'extraSpace': 25}).data('AutoResizer').check();
        $('.topic-editor-item').show();
        return false;
    };
    $('.topic-title .edit-link').live('click', Converse.topic.show_topic_editor);

    Converse.topic.cancel_topic_edit = function() {
        var summary = $('.topic-summary');
        $('.topic-editor-item').hide();
        var editor = $('#topic-summary-editor');
        editor.val(editor.attr('original'));
        summary.show();
        $('.topic-title .edit-link').show();
        $('#title-content').show();
    };
    $('.topic-editor .cancel-button').live('click', Converse.topic.cancel_topic_edit);

    Converse.topic.submit_topic_edit = function() {
        var that = $(this);
        var summary = $('#topic-summary-editor');
        var title = $('#title-editor');

        if(summary.val() == summary.attr('original') && title.val() == title.attr('original')) {
            // No change actually made, just cancel out.
            $('.topic-editor-item').hide();
            $('#title-content').show();
            $('.topic-summary').show();
            return false;
        }

        $.ajax({
            'url': that.attr('action'),
            'type': 'POST',
            'data': {
                'ajax': true,
                'summary': summary.val(),
                'title': title.val()
            },
            'dataType': 'json',
            'success': function(result) {
                if (result && result.success) {
                    $('.topic-summary .contents').html(result.summary);
                    $('#title-content').html(result.title);
                    $('.slug').html(result.trunc);
                    $('.slug').attr('href', result.permalink);
                    $('.topic-summary').show();
                    $('#title-content').show();
                    $('.topic-title .edit-link').show();
                    $('.topic-editor-item').hide();
                } else if (result) {
                    $('.topic-errors').html(result.errors);
                } else {
                    $('.topic-errors').text("Oops. Something went wrong while saving your edits.");
                }
            },
            'error': function() {
                $('.topic-errors').text("Oops. Something went wrong while saving your edits.");
            }
        });
        return false;
    };
    $('.topic-editor').live('submit', Converse.topic.submit_topic_edit);

    Converse.topic.show_comment_editor = function() {
        var that = $(this).closest('.comment');
        var editor = that.find('.comment-editor textarea');
        that.find('.rendered-content').hide();
        editor.attr('original', editor.val());
        editor.autoResize({'extraSpace': 25}).data('AutoResizer').check();
        that.find('.comment-editor').show();
        return false;
    };
    $('.comment .edit-link').live('click', Converse.topic.show_comment_editor);

    Converse.topic.cancel_comment_edit = function() {
        var that = $(this).closest('.comment');
        that.find('.comment-editor').hide();
        var editor = that.find('.comment-editor textarea');
        editor.val(editor.attr('original'));
        that.find('.rendered-content').show();
    };
    $('.comment-editor .cancel-button').live('click', Converse.topic.cancel_comment_edit);

    Converse.topic.submit_comment_edit = function() {
        var that = $(this);
        var editor = that.find('textarea');

        console.log('Editor original: ' + editor.attr('original'));
        console.log('Editor new     : ' + editor.val());
        if(editor.val() == editor.attr('original')) {
            // No change actually made, just cancel out.
            that.hide();
            that.closest('comment').find('.rendered-content').show();
            return false;
        }

        $.ajax({
            'url': that.attr('action'),
            'type': 'POST',
            'data': {
                'ajax': true,
                'content': editor.val()
            },
            'dataType': 'html',
            'success': function(result) {
                that.closest('.comment').html($(result).contents());
            },
            'error': function() {
                that.find('.comment-errors').text("Oops. Something went wrong while saving your edits.");
            }
        });
        return false;
    };
    $('.comment-editor').live('submit', Converse.topic.submit_comment_edit);

    Converse.topic.show_editable_summary = function() {
        var that = $(this).closest('.subtopic');
        var static_summary = that.find('.static-summary');
        static_summary.hide();
        that.find('.edit-link').hide();
        that.find('.summary-editor').val(static_summary.text()).data('AutoResizer').check();
        that.find('.editable-summary').show();
        return false;
    };
    $('.subtopic .edit-link').live('click', Converse.topic.show_editable_summary);

    Converse.topic.lock = function() {
        $.ajax({
            'url': $(this).attr('href'),
            'type': 'POST',
            'data': {
                'ajax': true
            },
            'dataType': 'json',
            'success': function(result) {
                if(result && result.success) {
                    if (result.locked) {
                        $('#lock-status').addClass('topic-locked');
                        $('#lock-status').removeClass('topic-unlocked');
                    } else {
                        $('#lock-status').removeClass('topic-locked');
                        $('#lock-status').addClass('topic-unlocked');
                    }
                }
            }
        });
        return false;
    };

    $('.topic-lock-link').live('click', Converse.topic.lock);
    $('.topic-unlock-link').live('click', Converse.topic.lock);

    Converse.topic.moderate_topic = function() {
        $.ajax({
            'url': $(this).attr('href'),
            'type': 'POST',
            'data': {
                'ajax': true
            },
            'dataType': 'json',
            'success': function(result) {
                if(result && result.success) {
                    if (result.moderated) {
                        $('#lock-status').addClass('topic-moderated');
                        $('#lock-status').removeClass('topic-unmoderated');
                    } else {
                        $('#lock-status').removeClass('topic-moderated');
                        $('#lock-status').addClass('topic-unmoderated');
                    }
                }
            }
        });
        return false;
    };

    $('.topic-moderate-link').live('click', Converse.topic.moderate_topic);
    $('.topic-unmoderate-link').live('click', Converse.topic.moderate_topic);

    Converse.topic.moderate_tools = function() {
        var that = $(this);
        var comment_id = that.closest('.comment-moderation-tools').attr('id').match(/(\d+)/)[1];

        $.ajax({
            'url': $(this).attr('href'),
            'type': 'POST',
            'data': {
                'ajax': true
            },
            'dataType': 'json',
            'success': function(result) {
                if(result && result.success) {
                    var comment = $('#comment-' + comment_id);
                    if (result.active) {
                        comment.removeClass('deleted');
                    } else {
                        comment.addClass('deleted');
                    }
                    if (result.held ) {
                        comment.addClass('held');
                    } else {
                        comment.removeClass('held');
                    }
                }
            }
        });

        return false;
    };
    $('.moderation-button').live('click', Converse.topic.moderate_tools);

    $('.summary-editor').autoResize({'extraSpace': 25, 'maxWidth': 700});

    // Disable toggling of comment display when the user is clicking on the editable form elements
    $('.summary-editor').live('click', function() { return false; });
    $('.editable-summary input[type=submit]').live('click', function() {
        $(this).closest('.subtopic-edit-form').submit();
        return false;
    });

    Converse.topic.submit_subtopic_summary_edit = function() {
        var that = $(this);
        var new_value = that.find('.summary-editor').val();

        if(new_value == that.find('.static-summary').text()) {
            // No change actually made, just cancel out.
            that.find('.editable-summary').hide();
            that.find('.static-summary').show();
            that.find('.edit-link').show();
            return false;
        }

        $.ajax({
            'url': that.attr('action'),
            'type': 'POST',
            'data': {
                'ajax': true,
                'summary': new_value
            },
            'dataType': 'json',
            'success': function(result) {
                if(result && result.success) {
                    that.find('.editable-summary').hide();
                    that.find('.static-summary').text(result.summary).show();
                    that.find('.edit-link').show();
                }
            }
        });
        return false;
    };
    $('.subtopic-edit-form').live('submit', Converse.topic.submit_subtopic_summary_edit);

    Converse.topic.cancel_subtopic_summary_edit = function() {
        var that = $(this);
        that.find('.editable-summary').hide();
        that.find('.static-summary').show();
        that.find('.edit-link').show();
        return false;
    };
    $('.subtopic-edit-form').live('keydown', function(e) {
        // Only cancel if Esc is pressed.
        if(e.keyCode != 27) return true;
        return $.proxy(Converse.topic.cancel_subtopic_summary_edit, this)();
    });
    $('.editable-summary .cancel-button').live('click', function() {
        return $.proxy(
            Converse.topic.cancel_subtopic_summary_edit,
            $(this).closest('.subtopic')
        )();
    });

    Converse.topic.cancel_subtopic_add_child = function() {
        $(this).hide().find('.new-subtopic-summary').text('');
        return false;
    };
    $('.new-subtopic').live('keydown', function(e) {
        // Only cancel if Esc is pressed.
        if(e.keyCoDe != 27) return true;
        return $.proxy(Converse.topic.cancel_subtopic_add_child, this)();
    });
    $('.new-subtopic .cancel-button').live('click', function() {
        return $.proxy(
            Converse.topic.cancel_subtopic_add_child,
            $(this).closest('.new-subtopic')
        )();
    });
    $('.new-subtopic-summary').autoResize({'extraSpace': 25, 'maxWidth': 650});

    $('.subtopic .permalink').live('click', function() {
        window.location = $(this).attr('href');
    });

    // Expand a subtopic if it's in the hash
    // (and scroll to a comment, if also present)
    $(window).hashchange(function() {
        var hash_subtopic = /^(subtopic-\d+)(?:,(comment-\d+))?$/.exec(window.location.hash.substring(1));
        if(hash_subtopic) {
            var subtopic_e = $('#' + hash_subtopic[1]);
            if(subtopic_e.length) {
                if(hash_subtopic.length > 2) {
                    Converse.topic.focused_comment = hash_subtopic[2];
                }
                Converse.topic.force_open_subtopic = true;
                $.proxy(Converse.topic.onclick_subtopic_header, subtopic_e)();
            }
        }
    });

    $(window).hashchange(); // Trigger at page load

});
