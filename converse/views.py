from collections import defaultdict
import json

from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseServerError
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.conf import settings
from django.db.models import Max
from django.template.defaultfilters import truncatewords, linebreaks

from converse import models, forms

def index(request):
    topic_list = models.Topic.all_with_last_update().order_by('-last_update')
    paginator = Paginator(topic_list, 50)
    
    try:
        page = int(request.GET.get('page', 1))
    except ValueError:
        page = 1

    try:
        topics = paginator.page(page)
    except (EmptyPage, InvalidPage):
        topics = paginator.page(paginator.num_pages)

    if request.user.is_authenticated():
        # Fetch all of these at once and match them up, to minimize the
        # number of database queries required (this literally saves us
        # almost a second of CPU time).
        seen_list = models.LatestSeen.objects.filter(user=request.user,
            topic__in=[t.id for t in topics.object_list]).select_related('topic')
        topics_seen = dict((s.topic.id, s) for s in seen_list)
        for topic in topics.object_list:
            has_new_items = False
            if topic.id not in topics_seen:
                has_new_items = True
            elif topics_seen[topic.id].latest < topic.last_update:
                has_new_items = True
            setattr(topic, 'has_new_items', has_new_items)
    
    return render(request, 'converse/topic_index.html', {
        'topics': topics,
    })

def topic(request, topic_id, slug):
    topic = get_object_or_404(models.Topic, pk=topic_id)

    if request.user.is_authenticated():
        models.LatestSeen.mark_seen(request.user, topic=topic)
        subtopics_new = topic.subtopics_new(request.user)
    else:
        subtopics_new = set()

    return render(request, 'converse/topic.html', {
        'topic': topic,
        'subtopics_new': subtopics_new,
    })

def subtopic_comments(request, subtopic_id):
    subtopic = get_object_or_404(models.Subtopic, pk=subtopic_id)
    comments = subtopic.all_visible_comments(request.user).order_by('created')
    if request.user.is_authenticated():
        models.LatestSeen.mark_seen(request.user, subtopic=subtopic)

    if 'id_after' in request.GET:
        comments = comments.filter(id__gt=int(request.GET['id_after']))

    return render(request, 'converse/comment_list.html', {
        'comments': comments,
    })

def user(request, user_id, slug):
    user = get_object_or_404(User, pk=user_id)
    topic_list = models.Topic.all_with_last_update().filter(creator=user).order_by('-last_update')
    comment_list = models.Comment.all_with_last_update().filter(creator=user).order_by('-last_update')

    return render(request, 'converse/user_profile.html', {
        'user': user,
        'topic_list': topic_list,
        'comment_list': comment_list,
    })

@require_POST
def add_comment(request, subtopic_id):
    subtopic = get_object_or_404(models.Subtopic, pk=subtopic_id)
    if subtopic.topic.is_locked:
        if 'ajax' in request.POST:
            return HttpResponse(json.dumps({'success': False, 'errors': "Topic is locked."}),
                         mimetype="application/json");
        else:
            return redirect('converse.views.topic', subtopic.topic.id)

    if request.user.is_authenticated():
        comment = models.Comment(creator=request.user, subtopic=subtopic, 
            is_held=subtopic.topic.needs_moderation)
        form = forms.CommentForm(request.POST, instance=comment)

        try:
            form.save()
            result = {'success': True}
        except ValueError:
            result = {'success': False, 'errors': "Invalid (empty?) reply content. Please try again."}
    else:
        result = {'success': False, 'errors': "You must be logged in to post a comment."}

    if 'ajax' in request.POST:
        return HttpResponse(json.dumps(result), mimetype="application/json")
    else:
        return redirect('converse.views.topic', subtopic.topic.id)

@require_POST
def edit_topic(request, topic_id):
    topic = get_object_or_404(models.Topic, pk=topic_id)
    if topic.creator != request.user and not request.user.has_perm('converse.can_moderate'):
        return HttpResponseForbidden()

    topic.editor = request.user
    form = forms.TopicForm(request.POST, instance=topic)
    result = {}
    try:
        form.save()
        result['summary'] = linebreaks(topic.summary)
        result['title'] = topic.title
        result['permalink'] = topic.permalink()
        result['trunc'] = truncatewords(topic.title, 10)
        result['success'] = True
    except ValueError, e:
        result['success'] = False
        result['errors'] = form.errors
        print "errors: %s" % form.errors
    return HttpResponse(json.dumps(result), mimetype="application/json")

@require_POST
def edit_comment(request, comment_id):
    comment = get_object_or_404(models.Comment, pk=comment_id)
    if comment.creator != request.user and not request.user.has_perm('converse.can_moderate'):
        return HttpResponseForbidden()
    if comment.is_system:
        return HttpResponseForbidden()

    comment.editor = request.user
    form = forms.CommentForm(request.POST, instance=comment)
    try:
        form.save()
        return render(request, 'converse/comment_list.html', {
            'comments': [comment],
        })
    except ValueError:
        return HttpResponseServerError()

@require_POST
def edit_subtopic(request, subtopic_id):
    subtopic = get_object_or_404(models.Subtopic, pk=subtopic_id)
    if subtopic.topic.is_locked:
        if 'ajax' in request.POST:
            return HttpResponse(json.dumps({'success': False, 'errors': "Topic is locked."}),
                         mimetype="application/json")
        else:
            return redirect('converse.views.topic', subtopic.topic.id)

    if request.user.is_authenticated():
        subtopic.editor = request.user
        form = forms.SubtopicForm(request.POST, instance=subtopic)
        try:
            form.save()
            result = {'success': True, 'summary': subtopic.summary}
        except ValueError:
            result = {'success': False, 'errors': "Invalid (empty?) summary. Please try again."}
    else:
        result = {'success': False, 'errors': "You must be logged in to edit summaries."}

    if 'ajax' in request.POST:
        return HttpResponse(json.dumps(result), mimetype="application/json")
    else:
        return redirect('converse.views.topic', subtopic.topic.id)

def add_topic(request):
    if not request.user.is_authenticated():
        return redirect("%s?next=%s" % (settings.LOGIN_URL, reverse('converse.views.add_topic')))

    if request.POST:
        topic = models.Topic(creator=request.user)
        form = forms.TopicForm(request.POST, instance=topic)
        try:
            form.save()
            models.LatestSeen.mark_seen(request.user, topic=topic)
            return redirect(reverse('converse.views.topic', args=(topic.id,)))
        except ValueError:
            pass
    else:
        form = forms.TopicForm()

    return render(request, "converse/topic_new.html", {
        'form': form,
    })

@require_POST
def add_subtopic(request, topic_id, parent_id):
    topic = get_object_or_404(models.Topic, pk=topic_id)
    if topic.is_locked:
        return redirect('converse.views.topic', topic.id)

    if parent_id:
        parent = get_object_or_404(models.Subtopic, pk=parent_id)
    else:
        parent = None

    if not request.user.is_authenticated():
        return redirect("%s?next=%s" % (settings.LOGIN_URL, reverse('converse.views.topic', args=(topic.id,))))

    subtopic = models.Subtopic(topic=topic, parent=parent, creator=request.user)
    form = forms.SubtopicForm(request.POST, instance=subtopic)
    try:
        form.save()
    except ValueError:
        pass
    return redirect('converse.views.topic', topic.id)

@require_POST
def moderate_comment(request, comment_id, action):
    if not request.user.is_authenticated():
        return redirect("%s?next=%s" % (settings.LOGIN_URL, reverse('converse.views.topic', args=(topic.id,))))

    if not request.user.has_perm('converse.can_moderate'):
        return HttpResponseForbidden()

    comment = get_object_or_404(models.Comment, pk=comment_id)
    result = {'success': True}
    if action == 'accept':
        comment.is_held = False
        comment.is_active = True
        comment.save()
    elif action == 'reject':
        comment.is_held = False
        comment.is_active = False
        comment.save()
    else:
        result['success'] = False
        result['errors'] = 'unknown action'

    result['held'] = comment.is_held
    result['active'] = comment.is_active

    if 'ajax' in request.POST:
        return HttpResponse(json.dumps(result), mimetype="application/json")
    else:
        return comment.permalink

@require_POST
def moderate_topic(request, topic_id, action):
    if not request.user.is_authenticated():
        return redirect("%s?next=%s" % (settings.LOGIN_URL, reverse('converse.views.topic', args=(topic.id,))))

    if not request.user.has_perm('converse.can_moderate'):
        return HttpResponseForbidden()

    topic = get_object_or_404(models.Topic, pk=topic_id)
    result = {'success': True}
    if action == 'lock':
        topic.is_locked = True
        topic.save()
    elif action == 'unlock':
        topic.is_locked = False
        topic.save()
    elif action == 'hold':
        topic.needs_moderation = True
        topic.save()
    elif action == 'release':
        topic.needs_moderation = False
        topic.save()
    else:
        result = {'success': False, 'errors': 'unknown action'}
    
    result['locked'] = topic.is_locked
    result['moderated'] = topic.needs_moderation
    if 'ajax' in request.POST:
        return HttpResponse(json.dumps(result), mimetype="application/json")
    else:
        return redirect('converse.views.topic', topic.id)
