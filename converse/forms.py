from django import forms

from converse import models

class CommentForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields = ('content',)

class SubtopicForm(forms.ModelForm):
    class Meta:
        model = models.Subtopic
        fields = ('summary',)

class TopicForm(forms.ModelForm):
    class Meta:
        model = models.Topic
        fields = ('title', 'summary',)
