from django.test import TestCase
from django.contrib.auth.models import User

from converse.models import LatestSeen
from converse.models import Topic

class TopicTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user('test', 'test@example.com')

    def setUp(self):
        self.topic = Topic.objects.create(title="Test", summary="testing", creator=self.user)

    def _assert_approx_equal_time(self, a, b):
        diff = max(a,b) - min(a,b)
        self.assertEqual(diff.days, 0)
        assert abs(diff.seconds) <= 1

    def test_create(self):
        """Makes sure we can create a topic."""
        assert self.topic

    def test_new_post_no_editor(self):
        """Ensures that a newly-created topic doesn't have an editor set."""
        self.assertEqual(self.topic.editor, None)

    def test_new_post_timestamps(self):
        """
        Make sure that a newly created post has (approximately)
        equal creation and modification times. We can't check for
        exact equality due to how Django does auto_now/auto_now_add."""
        self._assert_approx_equal_time(self.topic.created, self.topic.modified)

    def test_permalink(self):
        """Check for a proper permalink."""
        expected_permalink = "/topic/%d/test/" % self.topic.id
        self.assertEqual(self.topic.permalink(), expected_permalink)

    def test_editlog(self):
        """Check for the presence of an edit log for topics."""
        self.assertEqual(self.topic.edit_set.count(), 1)

    def test_last_update(self):
        """Check for the existence of a last update field when using all_with_last_update."""
        objs = Topic.all_with_last_update().filter(pk=self.topic.id)
        self.assertEqual(len(objs), 1)
        self._assert_approx_equal_time(objs[0].last_update, objs[0].modified)

    def test_unicode(self):
        self.assertEqual(unicode(self.topic), u"Test")

    def test_has_new(self):
        assert self.topic.has_new(self.user)
        LatestSeen.mark_seen(self.user, self.topic)
        assert not self.topic.has_new(self.user)
