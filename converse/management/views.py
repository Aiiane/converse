import functools
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseServerError, HttpResponseNotFound
from django.core.urlresolvers import reverse

from converse import models, forms

def admin_only(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        if not request.user.is_superuser:
            return HttpResponseForbidden()
        return func(request, *args, **kwargs)
    return wrapper

@admin_only
def index(request):
    return render(request, "converse/management/index.html")
