from converse.models import Topic, Subtopic, Comment
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
import datetime
import random

class ConverseCommand(BaseCommand):
    text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut
              est quis mauris feugiat viverra id a neque. Etiam rhoncus tempor
              ipsum, vitae fringilla nibh tempus placerat. Maecenas eget turpis
              at neque placerat sodales. Suspendisse lobortis nisl id ligula
              dictum commodo. Fusce nibh lorem, venenatis eu elementum ac,
              pellentesque sed felis. Integer eget nunc metus. In eu orci
              velit. Morbi elementum lobortis eros, vel volutpat justo
              sollicitudin in. Vestibulum turpis dui, ultricies et tempus quis,
              hendrerit hendrerit justo. Nullam tempus suscipit purus nec
              lobortis.  Vestibulum rutrum fermentum consequat. Duis at velit
              ut ligula molestie ultricies at eu sapien. Vestibulum elementum
              commodo consectetur.""".split() * 10

    oldest = datetime.datetime(2009, 1, 1)
    newest = datetime.datetime.today()
    all_users = User.objects.all()
    all_topics = Topic.objects.all()
    all_subtopics = Subtopic.objects.all()
    all_comments = Comment.objects.all()

    def _words(self, num, newlines=False):
        words = random.sample(self.text, random.randint(1,num))
        if newlines:
            words = [ w + random.choice(["\n", "\n\n"]) if random.random() < 0.05 else w for w in words ]

        return " ".join(words)

    def _date(self, start=oldest, end=newest): 
        return start + datetime.timedelta(days=random.randint(0, (end.date() - start.date()).days)) 

    def _username(self):
        return "".join(random.sample("aaaaaaaabccddddeeeeeeeeeeeeffgghhhhhhiiiiiiillllmmnnnnnnooooooopprrrrrrssssssttttttttttuuvwwyy", 6))

    def _user(self):
        return random.sample(self.all_users, 1)[0]

