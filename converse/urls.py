from django.conf.urls.defaults import patterns, url, include

urlpatterns = patterns('converse.views',
    url(r'^$', 'index'),

    url(r'^topic/(?P<topic_id>\d+)/(?:(?P<slug>[\w-]+)/)?$', 'topic'),
    url(r'^topic/(?P<topic_id>\d+)/moderate/(?P<action>\w+)/$', 'moderate_topic'),
    url(r'^topic/add/$', 'add_topic'),
    url(r'^topic/edit/(?P<topic_id>\d+)/$', 'edit_topic'),

    url(r'^subtopic/comments/(?P<subtopic_id>\d+)/$', 'subtopic_comments'),
    url(r'^subtopic/add/(?P<topic_id>\d+)/(?:(?P<parent_id>\d+)/)?$', 'add_subtopic'),
    url(r'^subtopic/edit/(?P<subtopic_id>\d+)/$', 'edit_subtopic'),

    url(r'^comment/add/(?P<subtopic_id>\d+)/$', 'add_comment'),
    url(r'^comment/edit/(?P<comment_id>\d+)/$', 'edit_comment'),
    url(r'^comment/(?P<comment_id>\d+)/moderate/(?P<action>\w+)/$', 'moderate_comment'),

    url(r'^user/(?P<user_id>\d+)/(?:(?P<slug>\w+)/)?$', 'user'),
)

urlpatterns += patterns('',
    url(r'manage/', include('converse.management.urls')),
)
