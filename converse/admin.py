from django.contrib import admin
from converse import models

class TopicAdmin(admin.ModelAdmin):
    class SubtopicInline(admin.StackedInline):
        model = models.Subtopic
        fields = ['summary','creator']
        extra = 1
    inlines = [SubtopicInline]
    list_display = ['title', 'creator', 'created', 'summary']
    list_filter = ['created']
    ordering = ['-created']
admin.site.register(models.Topic, TopicAdmin)

class SubtopicAdmin(admin.ModelAdmin):
    list_display = ['summary', 'creator', 'created', 'editor', 'modified']
    list_filter = ['created', 'modified']
    ordering = ['-created']
admin.site.register(models.Subtopic, SubtopicAdmin)

class CommentAdmin(admin.ModelAdmin):
    list_display = ['content', 'created', 'creator']
    list_filter = ['created']
    ordering = ['-created']
admin.site.register(models.Comment, CommentAdmin)

class EditAdmin(admin.ModelAdmin):
    list_display = ['timestamp', 'user', 'topic', 'subtopic', 'comment']
    list_filter = ['timestamp']
    order = ['-timestamp']
admin.site.register(models.Edit, EditAdmin)
